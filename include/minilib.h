// importé dans :
// - gdt.c
// - main.c
// - minilib.c
// - mmu.c
// - tss.c
// - user.c

#ifndef CESTLAMINILIB
#define CESTLAMINILIB

#ifndef NULL
#define NULL ((void*)0)
#endif

extern void __assert__(int x, char *f, unsigned line);

#define assert(x) __assert__(x, __FILE__, __LINE__) 
void putc(int c);
void puts(char *s);
void clear_screen();
void puthex(unsigned aNumber);		/* print an Hex number on screen */
void putud(unsigned aNumber);       /* print a unsigned decimal on screen */
void memset(void *, int, int);

//INTERROGATION
void *u_alloc_page();
void u_free_page(void *page);

#endif
