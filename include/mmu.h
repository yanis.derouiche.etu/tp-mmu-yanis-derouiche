#ifndef truc

#define truc

#define CR0_PG	0x80000000
#define CR0_WP	0x00010000

// taille d'une page : 4ko
#define PAGE_SIZE	4096

#define PAGE_TABLE_FREE		2

#include "idt.h"

// Le page directory entry, répertorie toutes les pages en gros.
struct pde_s {
    int p:1; // présence ou non de la page
    int rw:1;
    int us:1;
    int pwt: 1;
    int pcd:1;
    int a:1;
    int unused2:1;
    int ps:1;
    int unused1:4;
    int address:20; // l'adresse physique de la page
} __attribute__((packed));

// La page table entry, une page quoi.
struct pte_s {
    int p:1;
    int rw:1;
    int us:1;
    int pwt: 1;
    int pcd:1;
    int a:1;
    int d:1;
    int pat:1;
    int g:1;
    int unused1:3;
    int address:20;
} __attribute__((packed));

// nombre de page à avoir (voir mu.c)
#define PT_SIZE 1024

extern void setup_mmu();
extern unsigned int get_fault_address();
extern void flush_tlb();
extern void setup_page_table(int, struct pte_s[]);
extern void *page_to_addr(int page);
extern int addr_to_page(void *addr);
extern void page_fault_handler(int_regs_t *r);
extern void free_page_directory(struct pde_s *src_pd);
extern struct pte_s *clone_page_table(struct pte_s *src_pt);
extern void page_copy(void *page_src, void *page_dst);
extern void disable_paging();
extern void enable_paging();
extern void test_clone_page_table();
extern void test_clone_page_directory();

// INTERROGATION
extern void init_heap_page();
extern void test_alloc_and_free();


#endif
