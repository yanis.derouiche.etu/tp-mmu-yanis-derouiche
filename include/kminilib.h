// importé dans : 
// - kminilib.c
// - main.c
// - minilib.c

#ifndef LAKAMINILIB
#define LAKAMINILIB

#ifndef NULL
#define NULL ((void*)0)
#endif

extern void __assert__(int x, char *f, unsigned line);

#define assert(x) __assert__(x, __FILE__, __LINE__) 

void kclear_screen();				/* clear screen */
void kputc(char aChar);				/* print a single char on screen */
void kputs(char *aString);	
void kputhex(unsigned aNumber);	/* print a string on the screen */

// INTERROGATION
void k_free_page(void *page);	
void* k_alloc_page();
// pour éviter les conflits de nommage
void cc_free_page(void *page);
void* cc_alloc_page();

#endif
