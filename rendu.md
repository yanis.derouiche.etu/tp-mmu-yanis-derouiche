Ce fichier est le rendu de <PRENOM> <NOM> & <PRENOM> <NOM>
==========================================================

Définir des structures de données MMU
-------------------------------------




Définir un plan d'adressage
---------------------------

## Q5 Adresse de compilation

- La section .text est placé à l'adresse de phys (0x00100000)

- La section .data est juste après .text  
`. = ALIGN(4096);` fait en sorte que le début de la section suivante dans la mémoire soit bien un multiple de 4096

- la section .roadata est juste après .data (également alignée sur une frontière de 4096 octets)

- De même pour .bss juste après .roadata

- De même pour .stack placé juste après .bss

## Q6 Mémoire virtuelle et noyau

Pour ce qui est de l'adresse de my-kernel chargée en mémoire
idx1 = 0x00100000 >> 22 = 0x0
idx2 = (0x00100000 >> 12) & 0x3FF = 0x40l

Pour ce qui est de la mémoire vidéo
idx1 = 0xB8000 >> 22 = 0x2
idx2 = (0xB8000 >> 12) & 0x3FF = 0xE0

## Q7 Espace mémoire

ix1 fait 10 bits, 2^10 = 1024 donc il y aura 1024 page mémoire à configurer.
Or une page mémoire fait 4096 octets donc on aura 1024 * 4096 = 4 194 304 octets en mémoire.

## Q8 Plan d'adressage

Pour ne pas tout casser dans un premier temps il faudrait que chaque page pointe vers la vraie page physique. Comme si notre machine virtuelle n'existait pas pour le moment.

## Q12 plan d’adressage user

La plage d'adresses virtuelles configurée en mode utilisateur commence à l'adresse 0x400000 et se termine à 0x7FFFFF (entre 4 et 8-1 Mo).
 
Donc la plus petite adresse est 0x400000 et la plus grande adresse est 0x7FFFFF, logiquement.

## Q16 activation user land
L'activation du userland me donne "Caught fault, halting. Page Fault"

## Q18 chercher la faute

Je me retrouve avec Page fault at 0x101014

# Q22

Pas moyen de lancer des fragments arbitraires de code comme ça.

# Q29 

Pour éviter d'utiliser la MMU afin de détecter les débordements on pourrait regarder la pile à chaque execution de focntions et vérifier si elle déborde ou non. Mais ça prendra des ressources en plus par rapport à si l'on optait pour une MMU.

# 

Un premier programme _user_
---------------------------

## Q12 Plan d'adressage _user_

%% à completer

## Question Premier programme _user_ #6

%% à completer


