#include "minilib.h"

char user_stack[65536] __attribute__((aligned(4096)));

// char *user_stack = (char *) 0x800000;

extern int _begin_of_user_space;
extern int _end_of_user_space;

void recursive_funct() {
	int array[100];
	recursive_funct();
}

void user_entry() {
	clear_screen();
	puts("begin of user space :\n");
	puthex((int) &_begin_of_user_space);
	puts("\n");

	puts("end of user space :\n");
	puthex((int) &_end_of_user_space);
	puts("\n");

	puts("Hello from userland\n");
	//putc('B');
	putc('\n');

	// avec cette adresse ça fail pas parce qu'on est dans le champ d'adresse de l'user 
	// char *p = (char *)0x4F0000;
	// *p = 10;

	// ici ça plante parce qu'on est dans le champ d'adresse du kernel et plus celui du user
	// char *q = (char *)0x800000;
	// *q = 100;

	// mais le but c'est que ça plante aussi avec le premier cas parce que c'est un champ qu'on utilise pas normalement 
	// init page_table_user[i].p = 0 dans le setup_mmu pour dire que ça existe pas et du coup qu'on puisse pas y utililser.

	//puts("Test debordement de pile\n");
	//recursive_funct();

	//INTERROGATION

	u_alloc_page();
	u_free_page((void*) 123 );	// affiche des caractères spéciaux moche MAIS MARCHE !

	for(;;);
}
