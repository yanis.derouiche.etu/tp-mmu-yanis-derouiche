	#include "kminilib.h"
	#include "tss.h"
	#include "mmu.h"

	#define MAX_PAGES 4096
	#define STACK_LIMIT (1024 * 1024) // 1 Mo

	#define idx1 1
	#define idx2 1023
	#define offset 4096

	// Ce répertoire de page contient donc 1024 pages (et chacune de 4ko)
	// l'attribut d'alignement permet de s'assurer que le répertoire de page commence à une adresse multiple de 4096
	static struct pde_s page_dir[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));

	// INTERROGATION
	static struct pde_s initial_heap_page_table[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));
	
	// une page pour le kernel
	static struct pte_s page_table_kernel[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));
	
	// une page pour le user
	static struct pte_s page_table_user[PT_SIZE] __attribute__((aligned (PAGE_SIZE)));


	void *stack_addr;

	int page_status[MAX_PAGES];
	int first_free_page;

	extern int _begin_of_user_space;
	extern int _end_of_user_space;
	extern int STACK_SIZE;

	#define NUMBER_USER_PAGES ((&_end_of_user_space - &_begin_of_user_space) / PT_SIZE + 1)


	void *alloc_page() {
		int old_first_page = first_free_page;

		kputs("la premiere page libre : ");
		kputhex(first_free_page);
		kputs("\n");

		int next_page = page_status[first_free_page];
		page_status[first_free_page] = -1;
		first_free_page = next_page;
		return (void *)old_first_page;
	}

	void free_page(void *addr) {
		int page = addr_to_page(addr);
		if (page_status[page] == -2) {
			kputs("PROBLEME : page déjà liébrée\n");
		}
		// On remet la page comme libre en haut de la pile
		page_status[page] = first_free_page;
		first_free_page = page;
	}

	// on met toutes les pages restantes à libres
	void init_pages() {
		for (int i = first_free_page; i < MAX_PAGES; i++) {
			// Chaîner les pages libres, la dernière pointe vers -1
			if (page_status[i] != - 1) {
				page_status[i] = (i == MAX_PAGES - 1) ? -1 : i + 1;
			}
		}
	}
	
	// va mettre dans page_dir l'adresse de la table de pages
	// donc par ex le kernel aura un index de 0 et user 1
	void setup_page_table(int index, struct pte_s *ptable) {
		page_dir[index].ps = 0;
		page_dir[index].a = 0;
		page_dir[index].pcd = 0;
		page_dir[index].pwt = 0;
		page_dir[index].us = 1;
		page_dir[index].rw = 1;
		page_dir[index].p = 1;
		// convertit le pointeur en entier puis décale de 12 bits
		// (cad divise par 4096 pour avoir l'adresse de la table de pages)
		page_dir[index].address = ((int)ptable) >> 12;
	}

	void setup_mmu() {
		
		// idx1 = 1, idx2 = 1023
		// donc stack_addr stocke l'adresse de la stack du noyau
		stack_addr = page_to_addr((idx1 * 1024 + idx2) + 1);
		
		unsigned int i;
		// avec ça on saura si une page est libre ou non
		for (int i = 0; i < MAX_PAGES; i++) {
			page_status[i] = -1;
		}

		int user_pages = addr_to_page(&_end_of_user_space);

		// Kernel page table
		// On remplit la table de pages du noyau
		for (i = 0; i < PT_SIZE; i++) {
			page_table_kernel[i].g = 0;
			page_table_kernel[i].pat = 0;
			page_table_kernel[i].d = 0;
			page_table_kernel[i].a = 0;
			page_table_kernel[i].pcd = 0;
			page_table_kernel[i].pwt = 0;
			page_table_kernel[i].us = 0;
			page_table_kernel[i].rw = 1;
			page_table_kernel[i].p = 1;
			// pas de folie, on pointe sur la bonne adresse
			page_table_kernel[i].address = i;
			page_table_kernel[i].unused1 = 0;
			page_status[i] = -1;
		}

		// User page table
		// Remplir jusqu'au nombre de pages nécessaires => (_end_of_user_space - _begin_of_user_space) / PT_SIZE +1 au lieu de PT_SIZE

		for (i = 0; i < NUMBER_USER_PAGES; i++) {
			page_table_user[i].g = 0;
			page_table_user[i].pat = 0;
			page_table_user[i].d = 0;
			page_table_user[i].a = 0;
			page_table_user[i].pcd = 0;
			page_table_user[i].pwt = 0;
			page_table_user[i].us = 1;
			page_table_user[i].rw = 1;
			page_table_user[i].p = 1;
			page_table_user[i].address = i + PT_SIZE;
			page_table_user[i].unused1 = 0;
		}

		int last_user_page = PT_SIZE + NUMBER_USER_PAGES - 1; // pour calculer la première page libre en dessous
		first_free_page = last_user_page + 1; // La première page libre

		// Remplir le reste avec des pages non existantes
		for (i = NUMBER_USER_PAGES; i < PT_SIZE; i++) {
			page_table_user[i].g = 0;
			page_table_user[i].pat = 0;
			page_table_user[i].d = 0;
			page_table_user[i].a = 0;
			page_table_user[i].pcd = 0;
			page_table_user[i].pwt = 0;
			page_table_user[i].us = 1;
			page_table_user[i].rw = 1;
			page_table_user[i].p = 0;
			page_table_user[i].address = i + PT_SIZE;
			page_table_user[i].unused1 = 0;
		}

		// tester le debordement de la pile 
		// int j = ((int)user_stack) / 4096 - 1024;
		// page_table_user[j].p = 0;

		// on met toutes les pages restantes à libres
		init_pages();

		// 14 quand l'utilisateur essaye d'accéder illégalement à la mémoire
		idt_setup_int_handler(14, page_fault_handler);

		// dans le page_dir on met le kernel à l'adresse 0 et le user à 1
		setup_page_table(0, page_table_kernel);
		setup_page_table(1, page_table_user);

		// Les 12 bits de poids faible de CR3 doivent être à 0 car
		i = (int) page_dir;
		asm("movl %0, %%cr3"  : : "r"(i) :);

		// le bit 31 de CR0 indique si la MMU est activée
		// On va le mettre à 1 activé
		asm("movl %%cr0, %0" : "=r"(i) : :);
		i |= 0x80000000 | 0x00010000;
		asm("movl %0, %%cr0"  : : "r"(i) :);
	}



	unsigned int get_fault_address() {
		unsigned int v;
		asm("movl %%cr2, %0" : "=r"(v) : :);
		return v;
	}

	void flush_tlb() {
		unsigned int v;
		asm("movl %%cr3, %0" : "=r"(v) : :);
		asm("movl %0, %%cr3"  : : "r"(v) :);
	}

	// Convertit un numéro de page en adresse mémoire
	// Juste en multipliant le numéro de page par la taille d'une page
	extern void *page_to_addr(int page) {
		return (void *)(page * 4096);
	}

	// Converti une adresse mémoire en numéro de page
	// Juste en divisant l'adresse par la taille d'une page
	extern int addr_to_page(void *addr) {
		return (int)((int) addr / 4096);
	}


	void page_fault_handler(int_regs_t *r) {
		// récupère l'adresse mémoire qui a causé l'erreur
		void *addr = (void *)get_fault_address();

		// si c'est un débordement de pile à la LIMITE on fait un stack overflow 
		if (stack_addr - addr >= STACK_LIMIT) {
			kputs("stack overflow : ");
			int page = addr_to_page(addr);
			kputhex((unsigned int)addr);
			kputs(" page : ");
			kputhex(page);
			kputc('\n');
			for(;;);
			assert(0);
		// si simple débordement de pile on va allouer une page en plus
		} else if (addr < stack_addr) {
			void *page = alloc_page();
			if (page == NULL) {
				kputs("out of memory\n");
				assert(0);
			}

			int page_num = addr_to_page(addr);
			struct pte_s new_page;
			new_page.p = 1; 
			new_page.rw = 1; 
			new_page.us = 1; 
			new_page.address = (unsigned int)page_to_addr(page_num);
			page_table_user[page_num] = new_page;
		} else {
			kputs("segmentation fault at address ");
			kputhex((unsigned int)addr);  
			assert(0);
		}

	}

	void disable_virtual_memory() {
		asm volatile (
			"mov %%cr0, %%eax\n\t"
			"and $0x7FFFEFFF, %%eax\n\t"
			"mov %%eax, %%cr0\n\t"
			:
			:
			: "eax"
		);
	}

	void enable_virtual_memory() {
		asm volatile (
			"mov %%cr0, %%eax\n\t"
			"or $0x80010000, %%eax\n\t"
			"mov %%eax, %%cr0\n\t"
			:
			:
			: "eax"
		);
	}

	// active la MMU (bit 31 à 1)
	void enable_paging() {
		unsigned int i;
		asm("movl %%cr0, %0" : "=r"(i) : :);
		i |= 0x80010000;
		asm("movl %0, %%cr0"  : : "r"(i) :);
	}

	// désactive la MMU (bit 31 à 0)
	void disable_paging() {
		unsigned int i;
		asm("movl %%cr0, %0" : "=r"(i) : :);
		i &= ~0x80010000;
		asm("movl %0, %%cr0"  : : "r"(i) :);
	}

	void page_copy(void *page_src, void *page_dst) {

			disable_paging();

		int *src = page_src;
		int *dst = page_dst;
		// combien de fois on doit copier 
		int i=PAGE_SIZE/sizeof(int);
		for( ; i>0 ; i--) {
			// ici on copie puis on incrémente
			int value = *src;
			*dst = value;
			src++;
			dst++;

		}

		enable_paging();

	}

	struct pte_s *clone_page_table(struct pte_s *src_pt) {
		struct pte_s *new_pt = (struct pte_s *)alloc_page();
		
		if (!new_pt) {
			kputs("ICi?\n");
			return NULL;
		}
		
		for (int i = 0; i < PT_SIZE; i++) {
			if (src_pt[i].p) {
				struct pte_s *copied_page = (struct pte_s *)alloc_page();
				if (!copied_page) {
					kputs("ICQDSi?\n");
					return NULL;
				}
				page_copy((char *)((unsigned int)src_pt[i].address * PAGE_SIZE), (char *)copied_page);
				new_pt[i].p = 1;
				new_pt[i].address = (unsigned int)copied_page;
			} else {
				new_pt[i].p = 0;
			}
		}
		
		return new_pt;
	}

	void page_copy_mmu_off(void *page_src, void *page_dst) {
		uint64_t *src=page_src;
		uint64_t *dst=page_dst;
		int i=PAGE_SIZE/sizeof(uint64_t);
		for(;i>0;i--) *dst++=*src++;
		}



	void free_page_directory(struct pde_s *src_pd) {
		if (!src_pd) {
			return;
		}

		for (int i = 0; i < PT_SIZE; i++) {
			if (src_pd[i].p && src_pd[i].us) {
				struct pte_s *user_page_table = (struct pte_s *)(src_pd[i].address << 12);
				free_page(user_page_table);
			}
		}

		free_page(src_pd);
	}




	int compare_page_tables(struct pte_s *table1, struct pte_s *table2) {
		enable_paging();

		int i;

		// Compare the two tables entry by entry
		for (i = 0; i < PT_SIZE; i++) {
			if (table1[i].p != table2[i].p ||
				table1[i].rw != table2[i].rw ||
				table1[i].us != table2[i].us ||
				table1[i].pwt != table2[i].pwt ||
				table1[i].pcd != table2[i].pcd ||
				table1[i].a != table2[i].a ||
				table1[i].d != table2[i].d ||
				table1[i].pat != table2[i].pat ||
				table1[i].g != table2[i].g ||
				table1[i].address != table2[i].address) {
				return -1;
			}
		}

		return 0;
		disable_paging();
	}

	void test_clone_page_table() {
		struct pte_s *cloned_table;
		cloned_table = clone_page_table(page_table_kernel);

		if (cloned_table == NULL) {
        	kputs("Failed: cloned_table is NULL\n");
        return;
    	}

		if (compare_page_tables(page_table_kernel, cloned_table) != 0) {
			kputs("Failed: cloned_table is not identical to page_table_kernel\n");
			return;
		}

		kputs("Passed: clone_page_table works correctly\n");


	}



	struct pde_s *clone_page_directory(struct pde_s* src_pd){
		// on va allouer un nouveau directory
		struct pde_s *copy = alloc_page();

		struct pte_s *user_page_table = alloc_page();
		struct pte_s *src_user_page_table;

		disable_paging();

		for(int i = 0; i < PT_SIZE; i++){ // on copie le kernel
			copy[i] = src_pd[i];
		}
		src_user_page_table = (struct pte_s *)(page_to_addr(src_pd[1].address)); // on alloue une autre page (pour avoir cette page, on prend l'ancienne page et on décale son adresse de la taille d'une page (4096))
		copy[1].address = addr_to_page((void *)(uintptr_t) user_page_table);
		for(int i = 0; i < PT_SIZE; i ++){
			user_page_table[i] = src_user_page_table[i];  // on copie le user page table
			// pour chaque page présente on va lui allouer une nouvelle page
			if(src_user_page_table[i].p){
				char * copy_page = alloc_page();
				for(int i = 0; i < PT_SIZE; i++){
					copy_page[i] = (char) (uintptr_t) page_to_addr(src_user_page_table[i].address);				}
			}
		}

		enable_paging();

		return copy;
	}

	int compare_page_directories(struct pde_s* pd1, struct pde_s* pd2) {
    for(int i = 0; i < PT_SIZE; i++) {
        if(pd1[i].address != pd2[i].address) {
            return -1;
        }
    }
    return 0;
}

	void test_clone_page_directory() {
		struct pde_s *original_directory = page_dir; // Remplacez par votre répertoire de pages original
		struct pde_s *cloned_directory;

		cloned_directory = clone_page_directory(original_directory);

		if (cloned_directory == NULL) {
			kputs("Failed: cloned_directory is NULL\n");
			return;
		}

		if (compare_page_directories(original_directory, cloned_directory) != 0) {
			kputs("Failed: cloned_directory is not identical to original_directory\n");
			return;
		}

		kputs("Passed: clone_page_directory works correctly\n");
	}

	// INTERROGATION
	void init_heap_page() {

		// index où l'on veut mettre la page
		// ici 4 car on veut que la page soit à la 5ème entrée
		int index = 4;

		initial_heap_page_table[index].ps = 0;
		initial_heap_page_table[index].a = 0;
		initial_heap_page_table[index].pcd = 0;
		initial_heap_page_table[index].pwt = 0;
		initial_heap_page_table[index].us = 0;
		initial_heap_page_table[index].rw = 0;
		initial_heap_page_table[index].p = 0;
		initial_heap_page_table[index].address = ((int) &init_heap_page) >> 12;
	}

	void * cc_alloc_page() {
		int old_first_page = first_free_page;
		int next_page = page_status[first_free_page];
		page_status[first_free_page] = -1;
		first_free_page = next_page;
		// return le numéro de la page que l'on a allouée
		return (void *)old_first_page;
	}

	void cc_free_page(void *addr) {
		int page = (int) addr / 4096;
		if (page_status[page] == -2) {
			kputs("PAGE DEJA DISPO");
			kputs("\n");
		}
		// On remet la page comme libre en haut de la pile
		page_status[page] = first_free_page;
		first_free_page = page;
	}

	// INTERROGATION
	void test_alloc_and_free() {

		kputs("First free page : ");
		kputhex(first_free_page);
		kputs("\n");

		void *page = cc_alloc_page();

		kputs("First free page : ");
		kputhex(first_free_page);
		kputs("\n");

		cc_free_page(page_to_addr((int)page));

		kputs("First free page : ");
		kputhex(first_free_page);
		kputs("\n");
	}








