#include "minilib.h"
#include "gdt.h"
#include "tss.h"
#include "mmu.h"

#define STACK_SIZE	65536

static struct tss_s tss __attribute__((aligned (PAGE_SIZE)));;

static char kernel_stack[STACK_SIZE] __attribute__((aligned (PAGE_SIZE)));;

void setup_tss(struct gdt_entry_s *e) {
	unsigned int tss_addr = (int) &tss;
	memset(&tss, 0, sizeof(tss));
	tss.ss0 = 0x10; /* kernel data segment */ 
	tss.esp0 = ((int) kernel_stack) + sizeof(kernel_stack) - 4;
	e->base_high = (tss_addr >> 24) & 0xFF;
	e->base_middle = (tss_addr >> 16) & 0xFF;
	e->base_low = tss_addr & 0xFFFF;
}
