#include "minilib.h"
#include "kminilib.h"
#include "ioport.h"

void __assert__(int x, char *f, unsigned line)
{
    if (!x) {
        puts("Assert violated: ");
        puts("File ");
        puts(f);
        puts(" at line ");
        putud(line);
        for(;;);
    }
}

/* print an number in hexa */
void puthex(unsigned aNumber) {
  static char *hex_digit="0123456789ABCDEF";
  int i;
  int started=0;
  for(i=28;i>=0;i-=4) {
    int k=(aNumber>>i)&0xF;
    if(k!=0 || started) {
      putc(hex_digit[k]);
      started=1;
    }
  }
  if(!started) putc('0');
}

void putud(unsigned aNumber)
{
    char s[16] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    int i = 0;
    while (aNumber > 0) {
        s[i] = '0' + (aNumber % 10);
        i++;
        aNumber /= 10;
    }
    while (i > 0) 
        putc(s[--i]);
    
}

/* print a char on the screen */
static int cursor_x=0;					/* here is the cursor position on X [0..79] */
static int cursor_y=0;					/* here is the cursor position on Y [0..24] */


void memset(void *buf, int c, int s) {
	char *b = buf;
	int i;
	for (i = 0; i < s; i++) {
		b[i] = c;
	}
}

void putc(int c) {
	// int $87 => interruption logicielle qui permet de passer en mode user et donc de faire le putc
	asm("movl $1, %%eax" "\n\t"
	 	"movl %0, %%edx" "\n\t"
		"int $87"
		:
		: "r"(c)
		: "eax", "edx");
}

void puts(char *c) {
	// int $87 => interruption logicielle qui permet de passer en mode user et donc de faire le puts 
	asm("movl $2, %%eax" "\n\t"
	 	"movl %0, %%edx" "\n\t"
		"int $87"
		:
		: "r"(c)
		: "eax", "edx");
}

void clear_screen() {
	// int $87 => interruption logicielle qui permet de passer en mode user et donc de faire le clear_screen 
	asm("movl $3, %%eax" "\n\t"
		"int $87"
		:
		:
		: "eax");
}

// INTERROGATION
void *u_alloc_page() {
    void *ret;
    asm("movl $4, %%eax" "\n\t"
        "int $87" "\n\t"
        "movl %%eax, %0"
        : "=r"(ret)
        :
        : "eax");
    return ret;
}

// INTERROGATION
void u_free_page(void *page) {
    asm("movl $5, %%eax" "\n\t"
        "movl %0, %%edx" "\n\t"
        "int $87"
        :
        : "r"(page)
        : "eax", "edx");
}
