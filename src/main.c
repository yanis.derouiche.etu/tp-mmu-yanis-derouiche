#include "ioport.h"
#include "gdt.h"
#include "idt.h"
#include "minilib.h"
#include "mmu.h"
#include "call_user.h"
#include "kminilib.h"

extern void user_entry(void);

void empty_irq(int_regs_t *r) {
}

extern char user_stack[];

void sys_handler(int_regs_t *r) { //suivant la valeur de eax, on appel putc, puts etc
    switch(r->eax) {
        case 1:
            kputc(r->edx);
            break;
        case 2 :
            kputs((char *)r->edx);
            break;
        case 3:
            kclear_screen();
            break;
        case 4:
            cc_alloc_page();
            break;
        case 5 :
            cc_free_page( (void*) r->edx);
            break;
        default:
            kputs("Unknown syscall\n");
            assert(0);
            break;
    }
    // for(;;);
}


// void page_fault_handler(int_regs_t *r) {
//     kputs("Page fault, fault address : ");

//     int faultaddr = get_fault_address();

//     puthex(faultaddr);
//     kputs("\n");

//     assert(0);
// }

/* multiboot entry-point with datastructure as arg. */
void main(unsigned int * mboot_info)
{
    /* clear the screen */
    kclear_screen();
    kputs("Early boot.\n"); 
    kputs("\t-> Setting up the GDT... ");
    gdt_init_default();
    kputs("OK\n");

    kputs("\t-> Setting up the IDT... ");
    setup_idt();
    kputs("OK\n");

    kputs("\n\n");

    idt_setup_irq_handler(0, empty_irq);
    idt_setup_irq_handler(1, empty_irq);

    // 87 : lorsqu'un programme utilisateur sollicite une fonction système
    idt_setup_int_handler(87, sys_handler);

    setup_mmu();

    // INTERROGATION
    init_heap_page();

    __asm volatile("sti");

    /* minimal setup done ! */
    kputs("hello\n");

    // Passer en modalité user
    //test_clone_page_table();
    //test_clone_page_directory();

    call_user(user_entry, &user_stack[65536-4]);
    //test_alloc_and_free();

    kputs("Going idle\n");
    for(;;) ; /* nothing more to do... really nothing ! */
}


